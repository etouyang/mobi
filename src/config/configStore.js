import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import { createEpicMiddleware } from 'redux-observable'
import { composeWithDevTools } from 'redux-devtools-extension';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import { reducer } from '../observable/index.reducer';
import { epic } from '../observable/index.epic';
import { api } from '../api/index.api';

const middlewareNav = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
);

const configReducer = {
    key: 'li-persist',
    storage,
    version: 1,
    whitelist: ['_persist'], // Need to whitelist _persist for later migrations -> see https://github.com/rt2zz/redux-persist/issues/584
    debug: true,
};

export const configureStore = () => {
    const middlewares = [
        createEpicMiddleware(
            epic(),
            {
                dependencies: api
            }
        )
    ];
    const middlewareEnhancer = applyMiddleware(...middlewares, middlewareNav);
    const enhancers = composeWithDevTools(middlewareEnhancer); //debugger shown
    const reducerMain = persistCombineReducers(configReducer, reducer);

    const store = createStore(reducerMain, enhancers);
    const persistor = persistStore(store);
    return {store, persistor};
};
