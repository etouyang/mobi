import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const Layout = {width, height};
export const Colors ={
    white: '#fff',
    appBgDefault: '#F6F9FC',
    appDefaultColor: '#58A2EC',
    transparent: 'transparent',
    appHintColor: '#999999',
    splitLineColor: '#ECECEC',
    appDefaultTextColor: '#374554',
    navTitleTextColor:'#374554',
    appContentGradientColor: ['#40bdea', '#58A2EC', '#669cf4', '#8185f7'],
    accountColor: ['#0C77ED', '#0DD6F2'],
    appDefaultBgColor: '#324050FF',
    appImportBgGradientColor: ['#0C77EDCC', '#0DD6F2CC'],
    navBgColor: '#F6F9FC',
    inputTextColor: '#232735',
    hintInputTextColor: '#9FA7B3',
    explainTextColor: '#AAAAAA',
    assetColors: ['#679DF8', '#62B192', '#DF606B', '#0C4380', '#FBC631', '#3295A0']
};

export const URL = {
    base_url: 'https://trueryy.com:3000/api',
    login_url: '/login',
    refresh_token_url: '/refresh_token',
};