
const httpRequest = (
    method,
    url,
    options,
) =>
    fetch(url, {
        method,
        headers: options && options.headers,
        body: options && options.body,
    }).then(errorIfHttpResponseNotOk);

const httpJsonRequest = (method, url, options) => {
    return httpRequest(method, url, toJsonOptions(options)).then(toJsonResponseBody);
};

const errorIfHttpResponseNotOk = (response) => {
    if (!response.ok) {
        throw new HttpError({
            status: response.status,
            statusText: response.statusText,
            url: response.url,
        });
    }
    return response;
};

const createHttp = request => ({
    get: (url, options) => request('get', url, options),
    post: (url, options) => request('post', url, options),
});

export const http = createHttp(httpRequest);
export const httpJson = createHttp(httpJsonRequest);


const toJsonOptions = (
    options
) => {
    if (options) {
        const { headers, body, ...rest } = options;
        return {
            ...rest,
            headers: {
                ...headers,
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: body ? JSON.stringify(body) : undefined,
        };
    }
    return undefined;
};

const toJsonResponseBody = response => {
    // Prevent JSON parse Error for "204 No Content"
    if (response.status === 204) {
        return undefined;
    }
    return response.json();
};

export class HttpError extends Error {
    name = 'HttpError';
    message = '';
    status = '';
    statusText = '';
    url = '';
    stack = null;

    constructor({ url, status, statusText }, message) {
        super(message, status);
        this.message =
            message || `HTTP ${status} ${JSON.stringify(statusText)} for ${url}.`;
        if (Object.prototype.hasOwnProperty.call(Error, 'captureStackTrace')) {
            // V8 only
            Error.captureStackTrace(this, HttpError);
        } else {
            this.stack = new Error(message).stack;
        }

        this.status = status;
        this.statusText = statusText;
        this.url = url;
    }
};