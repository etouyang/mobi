const App = {
    SYSTEM_INIT: 'system/system_init'
};

const Buy = {
    BUY_INIT: 'buy/buy_init'
};

const Account = {
    ACCOUNT_LOGIN: 'account/login:start',
    ACCOUNT_LOGIN_SUCCESS: 'account/login:success',
    ACCOUNT_LOGIN_FAIL: 'account/login:fail',
    ACCOUNT_REFRESH_TOKEN: 'account/refresh_login:start',
    ACCOUNT_REFRESH_TOKEN_SUCCESS: 'account/refresh_login:success',
    ACCOUNT_REFRESH_TOKEN_FAIL: 'account/refresh_login:fail',
};

export default {
    ...App,
    ...Buy,
    ...Account
}