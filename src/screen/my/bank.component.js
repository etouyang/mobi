import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

const backImg = require('../../assets/mobai/icon_return.png');
const cardFront = require('../../assets/mobai/icon_idcard1.png');
const cardBackground = require('../../assets/mobai/icon_idcard2.png');
const choose = require('../../assets/mobai/icon_choose_default.png');
const chooseActive = require('../../assets/mobai/icon_choose_active.png');
class BankContainer extends Component {
    static navigationOptions = {header: null};
    state = {
        click:true
    };
    render() {
        return (
            <KeyboardAwareScrollView style={styles.view} showsVerticalScrollIndicator={false}>
                <View style={{width: '100%', height: 65, flexDirection: 'row'}}>
                    <TouchableOpacity style={{marginTop: 30, marginLeft: 20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width: 25, height: 25}}/>
                    </TouchableOpacity>
                    <Text style={{
                        marginTop: 32,
                        marginLeft: 20,
                        color: '#4E525F',
                        fontWeight: 'bold',
                        fontSize: 18
                    }}>授权环交所</Text>
                </View>
                <View style={{marginTop:5, marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>姓名</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'马'}
                        keyboardType={'default'}/>
                </View>
                <View style={{marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>身份证号</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'123456 1234 1234 123'}
                        keyboardType={'default'}/>
                </View>
                <Text style={{marginTop:25, marginLeft:20, fontSize:12, color:'rgba(0,0,0,0.4)'}}>中国大陆身份证验证</Text>
                <View style={{marginTop:20, marginLeft:20, height:160, flexDirection:'row', justifyContent:'space-around'}}>
                    <TouchableOpacity style={{flex:1, marginRight:15, backgroundColor:'#E1E5EB', borderRadius:8, alignItems:'center', justifyContent:'center'}}>
                        <Image source={cardFront} style={{width:50, height:50}}/>
                        <Text style={{fontSize:14, color:'#000000'}}>上传身份证正面</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1,marginRight:20, backgroundColor:'#E1E5EB', borderRadius:8, alignItems:'center', justifyContent:'center'}}>
                        <Image source={cardBackground} style={{width:50, height:50}}/>
                        <Text style={{fontSize:14, color:'#000000'}}>上传身份证背面</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:125, marginBottom:12, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>this.setState({click: !this.state.click})}>
                        {this.state.click ? <Image source={chooseActive} style={{width:17, height:17}}/> : <Image source={choose} style={{width:17, height:17}}/>}
                    </TouchableOpacity>
                    <Text style={{marginLeft:5, fontSize:12, color:'#000'}}>本人已详细阅读并同意签署全部</Text>
                    <Text style={{marginLeft:2, fontSize:12, color:'#E23D0D'}}>开户文件</Text>
                </View>
                <View style={{flex:1, justifyContent:'flex-end'}}>
                    <TouchableOpacity style={{marginLeft:30, marginRight:30, marginBottom:20, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}
                                    onPress={()=>this.props.main()}>
                        <Text style={{fontSize:14, color:'#FFF'}}>授权</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
    amount: {
        width:'70%',
        fontSize:16,
        fontWeight:'bold',
        textAlign: 'left',
        color:'#000',
        height:32,
        marginTop:5,
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
        main: () => dispatch(NavigationActions.navigate({routeName:'Tab'}))
    }
};

export default Bank = connect(mapStateToProps,mapDispatchToProps)(BankContainer)