import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Overlay} from "teaset";
import {Layout} from "../../config/configDefault";

const backImg = require('../../assets/mobai/icon_return.png');
const choose = require('../../assets/mobai/icon_choose_default.png');
const chooseActive = require('../../assets/mobai/icon_choose_active.png');
class CardContainer extends Component {
    static navigationOptions = {header: null};
    state = {
        click: true,
        key:null
    };

    _overlayView = () => {
        return (
            <Overlay.View
                style={{alignItems: 'center', justifyContent: 'center'}}
                modal={true}
                overlayOpacity={0.5}
            >
                <View style={{
                    backgroundColor: '#FFFFFF',
                    width: Layout.width - 60,
                    height: 360,
                    borderRadius: 8
                }}>
                    <View style={{marginTop:30, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:16, color:'#000', fontWeight:'bold'}}>验证手机号</Text>
                        <Text style={{marginTop:30, fontSize:12, color:'rgba(0,0,0,0.6)'}}>已发送验证码到188 **** 8888</Text>
                    </View>
                    <View style={{marginTop:80, height:25, alignItems:'center', justifyContent:'center'}}>
                        <TextInput
                            placeholder=""
                            style={styles.phoneCode}
                            underlineColorAndroid={'transparent'}
                            defaultValue={'123456'}
                            keyboardType={'default'}/>
                    </View>
                    <TouchableOpacity style={{marginTop:100, marginLeft:20, marginRight:20, height:48, backgroundColor:'#E23D0D', borderRadius:24, alignItems:'center', justifyContent:'center'}}
                                      onPress={() => {
                                          Overlay.hide(this.state.key);
                                          this.props.success();
                                      }}>
                        <Text style={{fontSize:14, color:'#FFF'}}>提交</Text>
                    </TouchableOpacity>
                </View>
            </Overlay.View>
        )
    };


    render() {
        return (
            <KeyboardAwareScrollView style={styles.view} showsVerticalScrollIndicator={false}>
                <View style={{width: '100%', height: 65, flexDirection: 'row'}}>
                    <TouchableOpacity style={{marginTop: 30, marginLeft: 20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width: 25, height: 25}}/>
                    </TouchableOpacity>
                    <Text style={{
                        marginTop: 32,
                        marginLeft: 20,
                        color: '#4E525F',
                        fontWeight: 'bold',
                        fontSize: 18
                    }}>银行卡开户</Text>
                </View>
                <View style={{marginTop:20, marginLeft:20, marginRight:20, height:54, borderRadius:8, backgroundColor:'#E1E5EB', alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:16, color:'#000', fontWeight:'bold'}}>开通前需要验证一张现有的银行卡</Text>
                </View>
                <View style={{marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>姓名</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'马'}
                        keyboardType={'default'}/>
                </View>
                <View style={{marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>身份证号</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'123456 1234 1234 123'}
                        keyboardType={'default'}/>
                </View>
                <View style={{marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>现有银行卡</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'8888 8888 8888 8888'}
                        keyboardType={'numeric'}/>
                </View>
                <View style={{marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>银行预留手机</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'188 8888 8888'}
                        keyboardType={'numeric'}/>
                </View>
                <View style={{marginTop:105, marginBottom:12, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>this.setState({click: !this.state.click})}>
                        {this.state.click ? <Image source={chooseActive} style={{width:17, height:17}}/> : <Image source={choose} style={{width:17, height:17}}/>}
                    </TouchableOpacity>
                    <Text style={{marginLeft:5, fontSize:12, color:'#000'}}>本人已详细阅读并同意签署全部</Text>
                    <Text style={{marginLeft:2, fontSize:12, color:'#E23D0D'}}>开户文件</Text>
                </View>
                <View style={{flex:1, justifyContent:'flex-end'}}>
                    <TouchableOpacity style={{marginLeft:30, marginRight:30, marginBottom:20, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}
                                      onPress={() => {
                                          let key = Overlay.show(this._overlayView());
                                          this.setState({key});
                                      }}>
                        <Text style={{fontSize:14, color:'#FFF'}}>开始验证</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
    amount: {
        width:'70%',
        fontSize:16,
        fontWeight:'bold',
        textAlign: 'left',
        color:'#000',
        height:32,
        marginTop:5,
    },
    phoneCode: {
        width:100,
        fontSize:22,
        fontWeight:'bold',
        textAlign: 'center',
        color:'#000',
        height:25,
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
        success: () => dispatch(NavigationActions.navigate({routeName:'Success'})),
    }
};

export default Card = connect(mapStateToProps,mapDispatchToProps)(CardContainer)