import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Overlay} from "teaset";
import {Layout} from "../../config/configDefault";

const succImg = require('../../assets/mobai/icon_succeed.png');

class SuccContainer extends Component {
    static navigationOptions = {header: null};
    state = {
        click: true,
        key: null
    };
    render() {
        return(
            <View style={styles.view}>
                <View style={{marginTop:129, height:240, alignItems:'center', justifyContent:'center'}}>
                    <View style={{width:240, height:240, alignItems:'center', backgroundColor:'#FFF', borderRadius:8}}>
                        <Image source={succImg} style={{width:102, height:102}}/>
                        <Text style={{marginTop:30, fontSize:14, color:'#000'}}>开户成功</Text>
                    </View>
                </View>
                <View style={{flex:1, justifyContent:'flex-end'}}>
                    <TouchableOpacity style={{marginLeft:30, marginRight:30, marginBottom:20, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}
                                      onPress={() => {
                                          this.props.bank();
                                      }}>
                        <Text style={{fontSize:14, color:'#FFF'}}>下一步, 授权环交所</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    }
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        bank: () => dispatch(NavigationActions.navigate({routeName:'Bank'})),
    }
};

export default Success = connect(mapStateToProps,mapDispatchToProps)(SuccContainer)