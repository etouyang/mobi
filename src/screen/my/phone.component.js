import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {Layout} from "../../config/configDefault";

const backImg = require('../../assets/mobai/icon_return.png');
class PhoneContainer extends Component {
    static navigationOptions = {header: null};
    render() {
        return (
            <View style={styles.view}>
                <View style={{width:'100%', height:65, flexDirection:'row'}}>
                    <TouchableOpacity style={{marginTop:30, marginLeft:20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    <Text style={{marginTop:32, marginLeft:20, color:'#4E525F', fontWeight:'bold', fontSize:18}}>验证手机</Text>
                </View>
                <View style={{marginTop:15, marginLeft:30, marginRight:30, height:83}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>手机号码</Text>
                    <View style={{flexDirection:'row'}}>
                        <TextInput
                            placeholder=""
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            defaultValue={'188 8888 8888'}
                            keyboardType={'numeric'}/>
                        <TouchableOpacity style={{marginLeft:5, width:'28%', height:32, marginTop:5, borderRadius:8, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontSize:12, color:'#FFF'}}>发送验证码</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginLeft:30, marginRight:130, height:1, backgroundColor:'rgba(0,0,0,0.05)'}}/>
                <View style={{marginTop:15, marginLeft:30, marginRight:30, height:83, borderBottomWidth:1,borderColor:'rgba(0,0,0,0.05)'}}>
                    <Text style={{marginTop:25, fontSize:12, color:'rgba(0,0,0,0.4)'}}>验证码</Text>
                    <TextInput
                        placeholder=""
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        defaultValue={'123456'}
                        keyboardType={'numeric'}/>
                </View>
                <View style={{flex:1, justifyContent:'flex-end'}}>
                    <TouchableOpacity style={{marginLeft:30, marginRight:30, marginBottom:20, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:14, color:'#FFF'}}>下一步, 授权环交所</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
    amount: {
        width:'70%',
        fontSize:16,
        textAlign: 'left',
        color:'#000',
        height:32,
        marginTop:5,
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
    }
};

export default phone = connect(mapStateToProps,mapDispatchToProps)(PhoneContainer)