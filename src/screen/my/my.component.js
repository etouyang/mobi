import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, StyleSheet } from 'react-native';
import {Layout, Colors} from "../../config/configDefault";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import LinearGradient from "react-native-linear-gradient";

const checkImg = require('../../assets/mobai/icon_certification.png');
const transferImg = require('../../assets/mobai/icon_history.png');
const accountImg = require('../../assets/mobai/icon_baixin.png');
const helpImg = require('../../assets/mobai/icon_help.png');
const callImg = require('../../assets/mobai/icon_phone.png');
const aboutImg = require('../../assets/mobai/icon_aboutme.png');
const setImg = require('../../assets/mobai/icon_setup.png');
const moreImg = require('../../assets/mobai/smallArrow.png');
const myImg = require('../../assets/check/icon_didi.png');
class My extends Component {
    static navigationOptions = {header: null};
    state={
        mylist: ['授权查看', '交易记录', '百信银行账户', '帮助中心', '联系客服', '关于我们'],
        images: [checkImg, transferImg, accountImg, helpImg, callImg, aboutImg]
    };
    render() {
        return (
            <View style={styles.view}>
                <FlatList
                    data={this.state.mylist}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._flatItem}
                    ListHeaderComponent={this._frontView}
                    ItemSeparatorComponent={this._spaceComponent}
                />
            </View>
        )
    }

    _keyExtractor = (item, index) => index.toString();

    _frontView = () => {
        return (
            <View style={{backgroundColor:'#F8F9FB'}}>
                <View style={{width:'100%', height:65, backgroundColor:'#FFF'}}>
                    <Text style={{marginLeft:20, marginTop:32, color:'#4E525F', fontWeight:'bold', fontSize:18}}>消息资讯</Text>
                </View>
                <View style={{marginTop:10, marginLeft:20, marginRight:20, height:88, borderRadius:8, backgroundColor:'#FFF', flexDirection:'row', alignItems:'center'}}>
                    <Image source={myImg} style={{marginLeft:20, width:50, height:50, borderRadius:25}}/>
                    <TouchableOpacity style={{marginLeft:20}} onPress={() => this.props.phone()}>
                        <Text style={{color:'#E23D0D', fontSize:14}}>188 8888 8888</Text>
                        <View style={{height:17, width:45, marginTop:8, borderRadius:14, borderWidth:1, borderColor:'#FFB700', justifyContent:'center'}}>
                            <Text style={{fontSize:12, color:'#FFB700', marginLeft:7, marginRight:7}}>lv 12</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end'}}>
                        <View style={{marginRight:20, width:50, height:50, borderRadius:25, backgroundColor:"#E23D0D", alignItems:'center', justifyContent:'center',}}>
                            <Image source={setImg} style={{width:25, height:25}}/>
                        </View>
                    </View>
                </View>
                <View style={{width:'100%', height:6}}/>
            </View>
        )
    };

    _spaceComponent= () => (
        <View style={{marginLeft:20, marginRight:20, height:1, backgroundColor:'rgba(0,0,0,0.05)'}}/>
    );

    _flatItem = ({item, index}) => {
        return(
            <TouchableOpacity style={[{width:'100%', height:75, flexDirection:'row', alignItems:'center', backgroundColor:'white'}, {marginTop: index ? null : 10}]}
                              onPress={()=> this.selectItem(index)}>
                <Image source={this.state.images[index]} style={{width:25, height:25, marginLeft:20}}/>
                <Text style={{marginLeft:30, fontSize:14, color:'#000000'}}>{item}</Text>
                <View style={{flex:1, flexDirection:'row', justifyContent:'flex-end', alignItems:'center'}}>
                    <Image source={moreImg} style={{width:9, height:13, marginRight:30}}/>
                </View>
            </TouchableOpacity>
        )
    };

    selectItem = (index) => {
        switch (index) {
            default: {
                this.props.openCard();
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        //...
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        phone: () => dispatch(NavigationActions.navigate({routeName:'Phone'})),
        openCard: () => dispatch(NavigationActions.navigate({routeName:'OpenCard'})),
    }
};

export default MyContainer = connect(mapStateToProps,mapDispatchToProps)(My);

const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor:'#F8F9FB'
    },
});
