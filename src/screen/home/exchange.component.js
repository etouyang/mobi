import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {Layout} from "../../config/configDefault";

const backImg = require('../../assets/mobai/icon_return.png');
class ExchangeContainer extends Component {
    static navigationOptions = {header: null};
    render() {
        return (
            <View style={styles.view}>
                <View style={{width:'100%', height:65, flexDirection:'row'}}>
                    <TouchableOpacity style={{marginTop:30, marginLeft:20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    <Text style={{marginTop:32, marginLeft:20, color:'#4E525F', fontWeight:'bold', fontSize:18}}>兑换碳A</Text>
                </View>
                <Text style={{marginTop:41, marginLeft:30, fontSize:10, color:'rgba(226, 61, 13, 0.4)'}}>兑换规则</Text>
                <Text style={{marginTop:10, marginLeft:30, fontSize:20, color:'rgb(226, 61, 13)', fontWeight:'bold'}}>1 积分 = 0.08 碳A</Text>
                <View style={{marginLeft:20, marginRight:20, height:116, backgroundColor:'#E1E5EB', borderRadius:8, flexDirection:'row'}}>
                    <View style={{flex:1}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10,marginTop:20, marginLeft:14}}>现有积分</Text>
                        <Text style={{color:'#000000', fontSize:20,marginTop:6, marginLeft:14, fontWeight:'bold'}}>4396.77</Text>
                    </View>
                    <View style={{flex:1}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10,marginTop:20, marginLeft:14}}>可用里程</Text>
                        <Text style={{color:'#000000', fontSize:20,marginTop:6, marginLeft:14, fontWeight:'bold'}}>240.77KM</Text>
                        <TouchableOpacity style={{marginTop:10, marginLeft:14, width:72, height:26, borderRadius:8, backgroundColor:'#FFF', alignItems:'center', justifyContent:'center' }}>
                            <Text style={{color:'#E23D0D', fontSize:12}}>全部兑换</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop:30, marginLeft:20, marginRight:20, height:62, backgroundColor:'#FFF', borderRadius:8, justifyContent:'center'}}>
                    <TextInput
                        placeholder="积分"
                        style={styles.amount}
                        underlineColorAndroid={'transparent'}
                        keyboardType={'numeric'}/>
                </View>
                <TouchableOpacity style={{marginLeft:30, marginRight:30, marginTop:40, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:14, color:'#FFF'}}>兑换</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
    amount: {
        marginLeft:20,
        fontSize:18,
        textAlign: 'left',
        color:'#E23D0D'
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
    }
};

export default Exchange = connect(mapStateToProps,mapDispatchToProps)(ExchangeContainer)