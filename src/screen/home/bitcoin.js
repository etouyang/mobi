import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {Layout} from "../../config/configDefault";
export const Bitcoin = (props) => {
    return (
        <View style={styles.container}>
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text style={styles.title}>BTC</Text>
                <Text>{'$6000'}</Text>
                <Text>{'+'+'10.00%'}</Text>
            </View>
            <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
                <Text style={styles.title}>ETH</Text>
                <Text>{'$500'}</Text>
                <Text>{'+'+'10.00%'}</Text>
            </View>
            <View style={styles.space}/>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: Layout.width-20,
        height: 80,
        marginLeft:10,
        flexDirection:'row',
        backgroundColor:'white'
    },
    title: {
        fontSize: 20,
        color: '#000000',
        fontWeight: 'bold'
    },
    space: {
        position:'absolute',
        marginLeft:Layout.width/2,
        marginTop:15,
        width:2,
        height:50,
        backgroundColor:'#efefef'
    }
});