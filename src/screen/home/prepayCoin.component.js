import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, Image} from 'react-native';
import {Layout} from "../../config/configDefault";
import ModalDropDown from '../../helpFuc/ModalDropDown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import { NavigationActions } from 'react-navigation';
import {connect} from 'react-redux';

const downImg = require('../../assets/images/icon_downloop.png');
class PrepayCoin extends React.Component{
    static navigationOptions = {title: '充值资产', headerBackTitle: null};
    state = {
        currentPay: 'BTC',
        pays: ['BTC', 'ETH'],
    };
    render() {
        return (
            <KeyboardAwareScrollView style={styles.keybordStyle}>
                <View style={styles.container}>
                    <View style={{backgroundColor:'#fdecc4', width:'100%', height:40}}>
                        <Text style={{marginLeft:20, marginTop:2, width:Layout.width-40, color:'#ef711a'}}>数字资产非存款, 实际金额会随着币价的波动而波动, 产品有风险, 购买需谨慎</Text>
                    </View>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{justifyContent:'center', height:50, width:100}}>
                            <Text style={{fontSize: 16}}>充值数字资产</Text>
                        </View>
                        <View style={styles.amountChoice}>
                            <ModalDropDown
                                ref='transferSelect'
                                options={this.state.pays}
                                defaultValue={this.state.currentPay}
                                textStyle={styles.choiceText}
                                dropdownStyle={styles.dropDownStyle}
                                onSelect={(idx, value) => {
                                    this.setState({currentPay: value}
                                    )}}
                            />
                            <TouchableOpacity onPress={()=>this.refs.transferSelect._onButtonPress()} style={styles.choiceBetween}>
                                <Image source={downImg} style={styles.choiceImg}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>充值地址</Text>
                        </View>
                        <View style={{flex:2, alignItems:'center', justifyContent:'center', marginRight:5}}>
                            <Text style={{fontWeight:'bold'}}>19bNgcTApEZANRx4sV4swELgySVUCtNPT7</Text>
                        </View>
                        <TouchableOpacity style={{flex:0.4, alignItems:'center', justifyContent:'center'}}>
                            <View style={{width:'100%', height:30, borderRadius:5, alignItems:'center', justifyContent:'center', backgroundColor:'#ef711a'}}>
                                <Text style={{color:'white'}}>复制</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>充值数量</Text>
                        </View>
                        <TextInput
                            placeholder="数量"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'numeric'}
                            onChangeText={(text) => this.setState({transferAmount: text})}
                            value={this.state.transferAmount}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>我的地址</Text>
                        </View>
                        <TextInput
                            placeholder="选填(或者在我的页面绑定地址)"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'numeric'}
                            onChangeText={(text) => this.setState({transferAmount: text})}
                            value={this.state.transferAmount}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{marginTop:5, width:Layout.width-40, marginLeft:20, height:230}}>
                        <Text style={{color:'#fb5c14'}}>{`1. 请勿向上述地址充值任何非${this.state.currentPay}资产，否则资产将不可找回;`}</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>{this.state.currentPay === 'BTC'?'2. 您充值至上述地址后，需要整个网络节点的确认，1次网络确认后到账，6次网络确认后可提币;' : '2.您充值至上述地址后，需要整个网络节点的确认，15次网络确认后到账，30次网络确认后可提币;'}</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>{this.state.currentPay === 'BTC' ? '3. 最小充值数量：0.001 BTC, 小于最小金额的充值将不会上账且无法退回;' : '3.最小充值数量：0.01 ETH, 小于最小金额的充值将不会上账且无法退回;'}</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>{this.state.currentPay === 'BTC' ? '4. 您可以在我的充值确认充值进度, 您也可以在BTC区块浏览器查看进度https://btc.com/' : '4. 平台会实时查看充值进度, 您也可以在Etherscan查看进度https://etherscan.io/'}</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>5. 请务必保证充值地址正确, 否则资产无法找回;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>6. 请务必确认电脑及浏览器安全，防止信息被篡改或泄露;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>7. 请务在我的页面绑定您的资产地址, 否则平台无法确定充值方;</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'flex-end'}}>
                        <TouchableOpacity style={styles.sendBtn} onPress={()=>this._transaction()}>
                            <Text style={styles.sendText}>提交订单</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }

    _transaction = () => {
        this.props.transfer();
    }
}

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        transfer: () => dispatch(NavigationActions.navigate({routeName:'PrepayCoinSuccess'}))
    }
};

export default PrepayCoinContainer = connect(mapStateToProps,mapDispatchToProps)(PrepayCoin)

const styles = StyleSheet.create({
    keybordStyle: {
        backgroundColor:'white'
    },
    container: {
        width:Layout.width,
        height:Layout.height - 60,
    },
    amountChoice: {
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'center',
    },
    choiceText: {
        color:'#415263',
        fontSize:16
    },
    dropDownStyle: {
        width: 100
    },
    choiceImg: {
        width:9,
        height:7
    },
    amount: {
        width:Layout.width-150,
        fontSize:16,
        textAlign: 'right',
        color:'#353A41'
    },
    sendBtn: {
        width:Layout.width-40,
        height:45,
        marginLeft:20,
        marginBottom:20,
        borderRadius:22.5,
        backgroundColor:'#ef711a',
        alignItems:'center',
        justifyContent:'center'
    },
    sendText: {
        color:'#FFFFFF',
        fontSize:16
    }
});