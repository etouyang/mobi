import React, { Component } from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Image, TextInput} from 'react-native';
import {Layout} from "../../config/configDefault";
import ModalDropDown from '../../helpFuc/ModalDropDown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const downImg = require('../../assets/images/icon_downloop.png');
class Sell extends Component{
    static navigationOptions = {title: '卖出资产'};
    state = {
        currentPay: 'BTC',
        pays: ['BTC', 'ETH'],
    };

    render() {
        return (
            <KeyboardAwareScrollView style={styles.keybordStyle}>
                <View style={styles.container}>
                    <View style={{backgroundColor:'#fdecc4', width:'100%', height:40}}>
                        <Text style={{marginLeft:20, width:Layout.width-40, marginTop:2, color:'#ef711a'}}>数字资产非存款, 实际金额会随着币价的波动而波动, 产品有风险, 购买需谨慎</Text>
                    </View>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{justifyContent:'center', height:50, width:100}}>
                            <Text style={{fontSize: 16}}>卖出数字资产</Text>
                        </View>
                        <View style={styles.amountChoice}>
                            <ModalDropDown
                                ref='transferSelect'
                                options={this.state.pays}
                                defaultValue={this.state.currentPay}
                                textStyle={styles.choiceText}
                                dropdownStyle={styles.dropDownStyle}
                                onSelect={(idx, value) => {
                                    this.setState({currentPay: value}
                                    )}}
                            />
                            <TouchableOpacity onPress={()=>this.refs.transferSelect._onButtonPress()} style={styles.choiceBetween}>
                                <Image source={downImg} style={styles.choiceImg}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>可用余额</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.001BTC</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>卖出数量</Text>
                        </View>
                        <TextInput
                            placeholder="数量"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'numeric'}
                            onChangeText={(text) => this.setState({transferAmount: text})}
                            value={this.state.transferAmount}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{marginTop:5, width:Layout.width-40, marginLeft:20, height:100}}>
                        <Text style={{color:'#fb5c14'}}>1. 当您提交订单后, 平台会在30分钟内卖出您的数字资产, 具体价格以实际成交为准;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>2. 成交完成后, 会将金额打入您的支付宝账户;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>3. 您可以在我的交易查看最新进展.</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'flex-end'}}>
                        <TouchableOpacity style={styles.sendBtn} onPress={()=>this._transfer()}>
                            <Text style={styles.sendText}>卖出</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }

}

export default Sell;


const styles = StyleSheet.create({
    keybordStyle: {
        backgroundColor:'white'
    },
    container: {
        width:Layout.width,
        height:Layout.height - 60,
    },
    amountChoice: {
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'center',
    },
    choiceText: {
        color:'#415263',
        fontSize:16
    },
    dropDownStyle: {
        width: 100
    },
    choiceImg: {
        width:9,
        height:7
    },
    amount: {
        width:Layout.width-150,
        fontSize:16,
        textAlign: 'right',
        color:'#353A41'
    },
    sendBtn: {
        width:Layout.width-40,
        height:45,
        marginLeft:20,
        marginBottom:20,
        borderRadius:22.5,
        backgroundColor:'#ef711a',
        alignItems:'center',
        justifyContent:'center'
    },
    sendText: {
        color:'#FFFFFF',
        fontSize:16
    },
});