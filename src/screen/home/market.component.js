import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {NavigationActions} from 'react-navigation';
import Echarts from 'native-echarts';

const backImg = require('../../assets/mobai/icon_return.png');
const buyImg = require('../../assets/mobai/icon_buy.png');
const sellImg = require('../../assets/mobai/icon_sell.png');
const sellActiveImg = require('../../assets/mobai/icon_sell_active.png');
const exchangeImg = require('../../assets/mobai/icon_exchange.png');
const topImg = require('../../assets/mobai/icon_rose.png');
const calImg = require('../../assets/mobai/icon_calculator.png');

class MarketContainer extends Component {
    static navigationOptions = {header: null};

    render() {
        let date = [];
        let data = [Math.random() * 300];
        for (let i = 1; i < 50; i++) {
            date.push(`${i}AM'`);
            data.push(Math.round((Math.random() - 0.5) * 20 + data[i - 1]));
        }
        const option = {
            //标题
            // title: {
            //     left: 'center',
            //     text: '大数据量面积图',
            // },
            //网格
            grid: {
                show:false,
                borderColor:'#fff',
                borderWidth:5,
            },
            //x轴
            xAxis: {
                //坐标轴在 grid 区域中的分隔线
                splitLine: {
                    show: false
                },
                //坐标轴类型
                type: 'category',
                //坐标轴两边留白策略
                boundaryGap: true,
                //是否显示坐标轴轴线
                axisLine: {
                    show:false
                },
                //坐标轴刻度
                axisTick: {
                    show:false
                },
                axisLabel: {
                    textStyle: {
                        fontSize : 20,
                        color: "rgba(0,0,0,0.8)",
                    },
                    // formatter: function (value) {
                    //     return value.substr(0,1)+'\\nn'+value.substring(1,value.length-1);
                    // }
                },
                axisPointer: {
                    snap: true
                },
                data: date
            },
            yAxis: {
                show:false
                // type: 'value',
                // boundaryGap: [0, '100%']
            },

            //点击某一个点的数据的时候，显示出悬浮窗
            tooltip: {
                trigger: 'axis',
                backgroundColor:'#FFF',
                textStyle: {
                    color: 'rgba(0,0,0,0.8)'
                },
                confine:true,
                // position: function (pt) {
                //     return [pt[0], '10%'];
                // }
            },
            dataZoom: [{
                show:false,
                type: 'inside',
                start: 0,
                end: 10
            }, {
                show:false,
                start: 0,
                end: 10,
            }],
            series: [
                {
                    name:'碳A',
                    type:'line',
                    clipOverflow:false,
                    areaStyle: {
                        normal: {
                            color: {
                                type: 'linear',
                                x: 0,
                                y: 0,
                                x2: 0,
                                y2: 1,
                                colorStops: [{
                                    offset: 0, color: 'rgba(226,61,13,0.1)' // 0% 处的颜色
                                }, {
                                    offset: 1, color: '#FFF' // 100% 处的颜色
                                }],
                                globalCoord: false // 缺省为 false
                            }
                        }
                    },
                    data: data
                }
            ]
        };

        return (
            <View style={styles.view}>
                <View style={{width:'100%', height:65, flexDirection:'row'}}>
                    <TouchableOpacity style={{marginTop:30, marginLeft:20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width:25, height:25}}/>
                    </TouchableOpacity>
                    <Text style={{marginTop:32, marginLeft:20, color:'#4E525F', fontWeight:'bold', fontSize:18}}>碳A行情</Text>
                </View>
                <View style={{marginLeft:20, marginRight:20, height:103, backgroundColor:'#FFFFFF', borderRadius:8}}>
                    <View style={{marginLeft:20, marginTop:13, flexDirection:'row', justifyContent:'space-between'}}>
                        <View >
                            <Text style={{color:'rgba(0,0,0,0.4)', fontSize:12}}>已有数量</Text>
                            <Text style={{color:'#E23D0D', fontSize:18}}>0.00</Text>
                        </View>
                        <View style={{marginRight:30}}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={{fontSize:26, color:'rgba(0,0,0,0.8)'}}>12.90</Text>
                                <Text style={{fontSize:10, color:'#000000', marginTop:5}}>￥</Text>
                            </View>
                            <View style={{marginTop:5, marginLeft:15, flexDirection:'row'}}>
                                <Text style={{fontSize:12, color:'#36A569', marginLeft:5}}>1.30%</Text>
                                <Image source={topImg} style={{width:16, height:14, marginLeft:6, marginTop:-1}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{flex:1, justifyContent:'flex-end', marginLeft:20}}>
                        <Text style={{color:'rgba(226,61,13,0.8)', fontSize:12, marginBottom:8}}>已拥有4396.77积分，可前往兑换。</Text>
                    </View>
                </View>
                <View style={{marginLeft:10, marginRight:10, marginTop:10, height:66, borderRadius:8, backgroundColor:'#E1E5EB', justifyContent:'space-around', flexDirection:'row', alignItems:'center'}}>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10}}>零点开盘</Text>
                        <Text style={{color:'rgba(0,0,0,0.6)', fontWeight:'bold', fontSize:16, marginTop:10}}>￥99.99</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10}}>最高</Text>
                        <Text style={{color:'rgba(0,0,0,0.6)', fontWeight:'bold', fontSize:16, marginTop:10}}>￥101.11</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10}}>最低</Text>
                        <Text style={{color:'rgba(0,0,0,0.6)', fontWeight:'bold', fontSize:16, marginTop:10}}>￥98.99</Text>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:10}}>成交量</Text>
                        <Text style={{color:'rgba(0,0,0,0.6)', fontWeight:'bold', fontSize:16, marginTop:10}}>1234.22</Text>
                    </View>
                </View>
                <View style={{marginTop:17, height:30, justifyContent:'space-around', flexDirection:'row'}}>
                    <TouchableOpacity style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:14}}>时</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:14}}>日</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1, alignItems:'center', borderBottomWidth:1, borderColor:'#E23D0D'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:14}}>周</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(0,0,0,0.4)', fontSize:14}}>月</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:30, height:240, width:'100%', }}>
                    <Echarts option={option} height={500} />
                </View>
                <View style={{flex:1, justifyContent:'flex-end'}}>
                    <View style={{height:50, flexDirection:'row'}}>
                        <TouchableOpacity style={{width:50, height:50,  borderRightWidth:1, borderColor:'rgba(0,0,0,0.05)', alignItems:'center', justifyContent:'center'}}
                                          onPress={() => this.props.exchange()}>
                            <Image source={calImg} style={{width:34, height:34}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, flexDirection:'row', borderRightWidth:1, borderColor:'rgba(0,0,0,0.05)', alignItems:'center', justifyContent:'center'}}
                                            onPress={() => this.props.exchange()}>
                            <Text style={{fontSize:14, color:'#E23D0D'}}>兑换</Text>
                            <Image source={exchangeImg} style={{width:20, height:20, marginLeft:20}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, flexDirection:'row', borderRightWidth:1, borderColor:'rgba(0,0,0,0.05)', alignItems:'center', justifyContent:'center'}}
                                          onPress={() => this.props.buy()}>
                            <Text style={{fontSize:14, color:'#E23D0D'}}>售出</Text>
                            <Image source={sellActiveImg} style={{width:20, height:20, marginLeft:20}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:1, flexDirection:'row', backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}
                                          onPress={() => this.props.buy()}>
                            <Text style={{fontSize:14, color:'#FFF'}}>买入</Text>
                            <Image source={buyImg} style={{width:20, height:20, marginLeft:20}}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
        exchange: () => dispatch(NavigationActions.navigate({routeName:'Exchange'})),
        buy: () => dispatch(NavigationActions.navigate({routeName:'Buy'})),
    }
};

export default Market = connect(mapStateToProps,mapDispatchToProps)(MarketContainer)