import React, {Component} from 'react';
import { View, Text, Image, StyleSheet} from 'react-native';

const bitImage = require('../../assets/images/bitcoin.png');
export default class GetCoinSuccess  extends React.Component{

    static navigationOptions = {title: '确认订单'};
    render() {
        return (
            <View style={{flex:1, alignItems:'center'}}>
                <Image source={bitImage} style={{width:120, height:120, marginTop:100}}/>
                <Text style={{color:'#fb5c14', marginTop:15}}>订单提交成功, 请在我的交易中查询进度</Text>
            </View>
        )
    }
}