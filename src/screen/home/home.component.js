import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import {NavigationActions} from 'react-navigation';
import {Layout} from "../../config/configDefault";

const frontImg = require('../../assets/mobai/smallArrow.png');
const topImg = require('../../assets/mobai/icon_rose.png');

const mobaiImg = require('../../assets/check/icon_mobike.png');
const maiImg = require('../../assets/check/icon_mdl.png');
const didiImg = require('../../assets/check/icon_didi.png');
const tanImg = require('../../assets/check/icon_tanA.png');
const cardImg = require('../../assets/check/icon_home_zxCard.png');
const readImg = require('../../assets/check/icon_home_academy.png');

class HomeContainer extends Component {
    static navigationOptions = {header: null};

    state={
        mylist: ['碳A', '摩拜单车月卡', '中信银行信用卡', '中信书院有声读物', '滴滴5元优惠劵', '麦当劳优惠劵'],
        images: [tanImg, mobaiImg, cardImg, readImg, didiImg, maiImg]
    };

    render() {

        return (
            <View style={styles.view}>
                {/*<View style={{marginLeft:20, marginRight:20,marginTop:34, height:50, flexDirection:'row', justifyContent:'space-between'}}>*/}
                    {/*<View>*/}
                        {/*<View style={{flexDirection:'row'}}>*/}
                            {/*<Text style={{fontSize:26, color:'#000000', fontWeight:'bold'}}>12.90</Text>*/}
                            {/*<Text style={{fontSize:10, color:'#000000', marginTop:5}}>￥</Text>*/}
                        {/*</View>*/}
                        {/*<View style={{marginTop:5, flexDirection:'row'}}>*/}
                            {/*<Text style={{fontSize:12, color:'#36A569', marginLeft:5}}>1.30%</Text>*/}
                            {/*<Image source={topImg} style={{width:16, height:14, marginLeft:6, marginTop:-1}}/>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                    {/*<TouchableOpacity style={{width:72, height:26, backgroundColor:'#E23D0D', borderRadius:8, alignItems:'center', justifyContent:'center'}}*/}
                                    {/*onPress={() => this.props.market()}>*/}
                        {/*<Text style={{fontSize:12, color:'#FFF'}}>查看行情</Text>*/}
                    {/*</TouchableOpacity>*/}
                {/*</View>*/}
                <FlatList
                    data={this.state.mylist}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._flatItem}
                    ListHeaderComponent={this._frontView}
                />

            </View>
        )
    };

    _keyExtractor = (item, index) => index.toString();
    _frontView = () => {
        return (
            <View style={{width:'100%', height:156+40+14+20, backgroundColor:'#FF6A3D'}}>
                <Text style={{marginLeft:20, marginTop:31, color:'#FFFFFF', fontWeight:'bold', fontSize:18}}>碳积分</Text>
                <View style={{marginTop:22, flexDirection:'row'}}>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(255,255,255,0.4)', fontSize:10}}>我的碳积分</Text>
                        <Text style={{marginTop:5, color:'#FFFFFF', fontWeight:'bold', fontSize:20}}>4396.77</Text>
                        <TouchableOpacity style={{marginTop:10, width:74, height:26, backgroundColor:'#E1E5EB', borderRadius:8, alignItems:'center', justifyContent:'center', flexDirection:'row'}}
                                          onPress={() => this.props.exchange()}>
                            <Text style={{color:'#000000', fontSize:12}}>兑换碳A</Text>
                            <View style={{alignItems:'center', justifyContent:'center'}}>
                                <Image source={frontImg} style={{width:14, height:14}}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1, alignItems:'center'}}>
                        <Text style={{color:'rgba(255,255,255,0.4)', fontSize:10}}>今日摩拜骑行里程</Text>
                        <Text style={{marginTop:5, color:'#FFFFFF', fontWeight:'bold', fontSize:20}}>4.70 KM</Text>
                        <TouchableOpacity style={{marginTop:10, width:74, height:26, backgroundColor:'#E1E5EB', borderRadius:8, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                            <Text style={{color:'#000000', fontSize:12}}>兑换积分</Text>
                            <View style={{ alignItems:'center', justifyContent:'center'}}>
                                <Image source={frontImg} style={{width:14, height:14}}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{position:'absolute', width:1, height:50, backgroundColor:'rgba(0,0,0,0.05)', left:'50%', top:21}}/>
                </View>
                <View style={{flex:1, marginTop:10, backgroundColor:'#F8F9FB'}}>
                    <Text style={{marginTop:40, marginLeft:20, fontSize:14, color:'rgba(0,0,0,0.4)'}}>权益列表</Text>
                </View>
            </View>
        )
    };

    _flatItem = ({item, index}) => {
        return (
            <TouchableOpacity style={{marginLeft:20, marginRight:20, height:90, flexDirection:'row'}}
                              onPress={()=> this.selectItem(index)}>
                <Image source={this.state.images[index]} style={{width:48, height:48, borderRadius:24, marginTop:10, zIndex:2}}/>
                <View style={{zIndex:1, marginLeft:-24, width:Layout.width-44-20, height:70, backgroundColor:'#FFF', borderRadius:8, justifyContent:'center'}}>
                    <View style={{flexDirection:'row', marginLeft:36, justifyContent:'space-between'}}>
                        <Text style={{fontSize:16, fontWeight:'bold'}}>{item}</Text>
                        <TouchableOpacity style={{height:22, marginTop:-3, marginRight:20, backgroundColor:'rgba(226,61,13,0.2)', borderRadius:12, alignItems:'center', justifyContent:'center'}}>
                            <Text style={{color:'#E23D0D', fontSize:12, marginLeft:11, marginRight:11,}}>2600积分</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={{marginLeft:36, marginTop:10, fontSize:12, color:'rgba(0,0,0,0.4)'}}>拥有数量: 2</Text>
                </View>
            </TouchableOpacity>
        )
    };
    selectItem = (index) => {
        switch (index) {
            default: {
                this.props.market()
                console.log(index)
            }
        }
    }
}

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        market: () => dispatch(NavigationActions.navigate({routeName:'Market'})),
        exchange: () => dispatch(NavigationActions.navigate({routeName:'Exchange'}))
    }
};

export default Home = connect(mapStateToProps,mapDispatchToProps)(HomeContainer)

const styles = StyleSheet.create({
    view: {
        flex:1,
        backgroundColor:'#F8F9FB'
    },
    contain: {
        width: Layout.width,
        height: Layout.width * 0.4 + 20,
        backgroundColor: '#F5FCFF'
    },
    slider: {
        width: Layout.width,
        height: Layout.width * 0.4,
        alignItems:'center',
        justifyContent:'center'
    },
    call: {
        width: '100%',
        height: '100%',
        // alignItems:'center',
        justifyContent:'center'
    },
    text: {
        color: '#000080',
        fontSize: 15,
        fontWeight: 'bold'
    },
});