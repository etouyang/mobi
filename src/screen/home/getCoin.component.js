import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import ModalDropDown from '../../helpFuc/ModalDropDown';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Layout} from "../../config/configDefault";
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";

const downImg = require('../../assets/images/icon_downloop.png');
class GetCoin extends Component{
    static navigationOptions = {title: '提取资产', headerBackTitle: null};
    state = {
        currentPay: 'BTC',
        pays: ['BTC', 'ETH'],
        address: null
    };

    render () {
        return (
            <KeyboardAwareScrollView style={styles.keybordStyle}>
                <View style={styles.container}>
                    <View style={{backgroundColor:'#fdecc4', width:'100%', height:40}}>
                        <Text style={{marginLeft:20, marginTop:2, width:Layout.width-40, color:'#ef711a'}}>数字资产非存款, 实际金额会随着币价的波动而波动, 产品有风险, 购买需谨慎</Text>
                    </View>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{justifyContent:'center', height:50, width:100}}>
                            <Text style={{fontSize: 16}}>提取资产</Text>
                        </View>
                        <View style={styles.amountChoice}>
                            <ModalDropDown
                                ref='transferSelect'
                                options={this.state.pays}
                                defaultValue={this.state.currentPay}
                                textStyle={styles.choiceText}
                                dropdownStyle={styles.dropDownStyle}
                                onSelect={(idx, value) => {
                                    this.setState({currentPay: value}
                                    )}}
                            />
                            <TouchableOpacity onPress={()=>this.refs.transferSelect._onButtonPress()} style={styles.choiceBetween}>
                                <Image source={downImg} style={styles.choiceImg}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>提取地址</Text>
                        </View>
                        <TextInput
                            placeholder="地址"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'default'}
                            onChangeText={(text) => this.setState({address: text})}
                            value={this.state.address}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>可用余额</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.001BTC</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>提取数量</Text>
                        </View>
                        <TextInput
                            placeholder="数量"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'numeric'}
                            onChangeText={(text) => this.setState({transferAmount: text})}
                            value={this.state.transferAmount}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>手续费</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.0015BTC/0.007ETH</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{marginTop:5, width:Layout.width-40, marginLeft:20, height:230}}>
                        <Text style={{color:'#fb5c14'}}>{this.state.currentPay === 'BTC' ? '1. 最小提币数量为：0.01 BTC' : '1. 最小提币数量为：0.02 ETH'}</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>2. 请不要直接提币到ICO的众筹地址，这会导致您无法收取众筹到的数字资产;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>3. 网络转账费用是不固定的，取决于转账时合约执行需要消耗的算力, 会随时变动;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>4. 请务必确认电脑及浏览器安全，防止信息被篡改或泄露;</Text>
                        <Text style={{color:'#fb5c14', marginTop:5}}>5. 请在我的页面交易查看进度.</Text>
                    </View>
                    <View style={{flex:1, justifyContent:'flex-end'}}>
                        <TouchableOpacity style={styles.sendBtn} onPress={()=>this._transaction()}>
                            <Text style={styles.sendText}>提交订单</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    }

    _transaction = () => {
        this.props.transfer();
    }
}

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        transfer: () => dispatch(NavigationActions.navigate({routeName:'GetCoinSuccess'}))
    }
};

export default GetCoinContainer = connect(mapStateToProps,mapDispatchToProps)(GetCoin);

const styles = StyleSheet.create({
    keybordStyle: {
        backgroundColor:'white'
    },
    container: {
        width:Layout.width,
        height:Layout.height - 60,
    },
    amountChoice: {
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'center',
    },
    choiceText: {
        color:'#415263',
        fontSize:16
    },
    dropDownStyle: {
        width: 100
    },
    choiceImg: {
        width:9,
        height:7
    },
    amount: {
        width:Layout.width-150,
        fontSize:16,
        textAlign: 'right',
        color:'#353A41'
    },
    sendBtn: {
        width:Layout.width-40,
        height:45,
        marginLeft:20,
        marginBottom:20,
        borderRadius:22.5,
        backgroundColor:'#ef711a',
        alignItems:'center',
        justifyContent:'center'
    },
    sendText: {
        color:'#FFFFFF',
        fontSize:16
    }
});
