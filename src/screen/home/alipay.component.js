import React, { Component } from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {Layout} from "../../config/configDefault";

const payImg = require('../../assets/images/alipay.png');
class Alipay extends Component {
    static navigationOptions = {
        title: '支付'
    };
    state = {
        startTime: parseInt(Date.now() / 1000),
        timePass: 1800
    };

    componentWillMount() {
        this.state.timer = setInterval(() => {
            if (this.state.timePass >=0) {
                let count = parseInt(Date.now() / 1000 - this.state.startTime);
                let pass = this.state.timePass-count;
                this.setState( {timePass: pass} );
            } else {
                clearInterval(this.state.timer);
            }
        }, 1000);
    };

    componentWillUnmount() {
        this.state.timer && clearInterval(this.state.timer);
    };

    render() {
        const {timePass} = this.state;
        let min = parseInt(timePass/60);
        let s = timePass%60;
        if (s < 10) s = '0' + s;
        return (
            <View style={{flex:1, backgroundColor:'white'}}>
                <View style={{backgroundColor:'#fdecc4', width:'100%', height:40}}>
                    <Text style={{marginLeft:20, marginTop:2, color:'#ef711a', width:Layout.width-40}}>请在30分钟内, 将资金转账到当前支付宝账户, 实时结果请在我的充值中查看</Text>
                </View>
                <Image source={payImg} style={{width:Layout.width-100, height:Layout.width-100, marginLeft:50, marginTop:50}}/>
                <View style={{width:'100%', height:40, marginTop:10, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold', fontSize:20}}>{min + ':' + s}</Text>
                </View>
                <TouchableOpacity style={{alignItems:'center', justifyContent:'center', width: '50%', height:30, marginLeft:'25%', borderRadius:15, backgroundColor:'#ef711a'}}>
                    <Text style={{color:'white'}}>保存付款二维码至相册</Text>
                </TouchableOpacity>
            </View>
        )
    }

}

export default Alipay;