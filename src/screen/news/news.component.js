import React, {Component} from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
import {Layout} from "../../config/configDefault";

class NewsContainer extends Component {
    static navigationOptions = {header:null};
    state = {
        swiperlist:['公告一', '公告二', '公告三', '公告四'],
        news:[{
            date:'2018/08/12',
            title:'关于 碳A产品 的介绍',
            detail:'文章内容文章内容文章内容文章内容文章内容'
        }, {
            date:'2018/08/12',
            title:'这是文章标题这是文章标题这是文章标题这是文章标题这是文章标题',
            detail:'文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容'
        },{
            date:'2018/08/12',
            title:'这是文章标题这是文章标题这是文章标题这是文章标题这是文章标题',
            detail:'文章内容文章内容文章内容文章内容文章内容文章内容文章内容文章内容'
        },{
            date:'2018/08/12',
            title:'关于 碳A产品 的介绍',
            detail:'文章内容文章内容文章内容文章内容文章内容'
        },]
    };

    render() {
        return (
            <View style={{backgroundColor:'#F8F9FB'}}>
                <FlatList
                    data={this.state.news}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._flatItem}
                    ListHeaderComponent={this._frontView}
                    ItemSeparatorComponent={this._spaceComponent}
                />
            </View>
        )
    }

    _frontView = () => {
        return (
            <View style={{backgroundColor:'#F8F9FB'}}>
                <View style={{width:'100%', height:65, backgroundColor:'#FFF'}}>
                    <Text style={{marginLeft:20, marginTop:32, color:'#4E525F', fontWeight:'bold', fontSize:18}}>消息资讯</Text>
                </View>
                <View style={styles.contain}>
                    <Swiper
                        showsButtons={false}
                        autoplay={true}
                        index={0}
                        autoplayTimeout={3}
                        paginationStyle={{bottom: 0}}>
                        {this.state.swiperlist.map((val, idx)=> {
                            return (
                                <View style={styles.slider} key={idx}>
                                    <Text style={styles.text}>{val}</Text>
                                </View>
                            )
                        })}
                    </Swiper>
                </View>
            </View>
        )
    };

    _keyExtractor = (item, index) => index.toString();

    _spaceComponent= () => (
        <View style={{width:'100%', height:1, backgroundColor:'rgba(0,0,0,0.05)'}}/>
    );

    _flatItem = ({item, index}) => {
        return (
            <View style={{marginLeft:20, marginRight:20, height:128}}>
                <View style={{flexDirection:'row', marginTop:20}}>
                    <Text style={{fontSize:12, color:'rgba(0,0,0,0.4)'}}>发布人</Text>
                    <View style={{marginLeft:10, marginRight:10, marginTop:2, width:1, height:8, backgroundColor:'rgba(0,0,0,0.4)'}}/>
                    <Text style={{fontSize:12, color:'rgba(0,0,0,0.4)'}}>{item.date}</Text>
                </View>
                <Text style={{fontSize:16, color:'#000', fontWeight:'bold', marginTop:10}} numberOfLines={2}>{item.title}</Text>
                <Text style={{fontSize:12, color:'rgba(0,0,0,0.4)', marginTop:10}} numberOfLines={1}>{item.detail}</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    contain: {
        marginLeft:20,
        marginRight:20,
        height: 158,
        backgroundColor:'#FFF'
    },
    slider: {
        width: '100%',
        height: 158,
        alignItems:'center',
        justifyContent:'center'
    },
    text: {
        color: '#000080',
        fontSize: 15,
        fontWeight: 'bold'
    },
});

export default NewsContainer;