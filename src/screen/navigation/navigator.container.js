import React from 'react';
import {addNavigationHelpers} from 'react-navigation';
import {connect} from 'react-redux';
import {createReduxBoundAddListener} from 'react-navigation-redux-helpers';
import AppNavigator from './navigator';

const addListener = createReduxBoundAddListener("root");
const mapStateToProps = state => ({
    nav: state.navigation.nav,
});

const mapDispatchToProps = dispatch => (
    Object.assign({dispatch: dispatch})
);

const AppWithNavigationState = ({dispatch, nav}) => (
    <AppNavigator navigation={addNavigationHelpers({dispatch, state: nav, addListener})}/>
);

export const AppNavigatorRedux = connect(mapStateToProps, mapDispatchToProps)(AppWithNavigationState);