import React from 'react';
import {Image, StyleSheet} from "react-native";
import {TabNavigator, TabBarBottom, StackNavigator} from 'react-navigation';

import Home from '../home/home.component';
// import Licai from '../licai/licai.component';
import News from '../news/news.component';
import My from '../my/my.component';
import Market from '../home/market.component';
import Exchange from '../home/exchange.component';
import Phone from '../my/phone.component';
import Bank from '../my/bank.component';
import Buy from '../licai/exchange.buy.component';
import OpenCard from '../my/openCard.component'
import Success from '../my/success.component';



// import Buy from '../licai/buy.component';
import Alipay from '../home/alipay.component';
import PayWithMoney from '../home/payWithMoney.component';
import Sell from '../home/sell.component';
import PrepayCoin from '../home/prepayCoin.component';
import PrepayCoinSuccess from '../home/prepayCoinSuccess.component';
import GetCoin from '../home/getCoin.component';
import { Layout, Colors } from '../../config/configDefault';
import GetCoinSuccess from "../home/getCoinSuccess.component";
import BuySuccess from "../licai/buySuccess.component";

const Tab = TabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                tabBarLabel: '首页',
                tabBarIcon: ({focused}) => (
                    <Icon
                        focused={focused}
                        file={require('../../assets/mobai/icon_home_default.png')}
                        selectedFile={require('../../assets/mobai/icon_home_active.png')}
                    />
                )
            },
        },
        // Licai: {
        //     screen:Licai,
        //     navigationOptions: {
        //         tabBarLabel: '理财',
        //         tabBarIcon: ({focused}) => {
        //             return (
        //                 <Icon
        //                     focused={focused}
        //                     file={require('../../assets/mobai/icon_rights_default.png')}
        //                     selectedFile={require('../../assets/mobai/icon_rights_active.png')}
        //                 />
        //             )
        //         }
        //     },
        // },
        News: {
            screen:News,
            navigationOptions: {
                tabBarLabel: '新闻',
                tabBarIcon: ({focused}) => {
                    return (
                        <Icon
                            focused={focused}
                            file={require('../../assets/mobai/icon_message_default.png')}
                            selectedFile={require('../../assets/mobai/icon_message_active.png')}
                        />
                    )
                }
            },
        },
        My: {
            screen:My,
            navigationOptions: {
                tabBarLabel: '设置',
                tabBarIcon: ({focused}) => (
                    <Icon
                        focused={focused}
                        file={require('../../assets/mobai/icon_person_default.png')}
                        selectedFile={require('../../assets/mobai/icon_person_active.png')}
                    />
                )
            },
        },
    },
    {
        tabBarPosition: 'bottom', // 设置tabBar的位置
        tabBarComponent: TabBarBottom,
        swipeEnabled: false, //是否允许在标签之间进行滑动
        animationEnabled: false, //是否在更改标签时显示动画。
        lazy: true, //懒加载
        initialLayout: {
            width: Layout.width,
            height: Layout.height
        },
        tabBarOptions: {
            activeBackgroundColor: Colors.white,
            activeTintColor: Colors.appDefaultColor,
            inactiveBackgroundColor: Colors.white,
            inactiveTintColor: Colors.appHintColor,
            showLabel: false
        }
    }
);

export default RootScreen = StackNavigator(
    {
        Tab: {
            screen: Tab
        },
        Alipay: {
            screen: Alipay
        },
        PayWithMoney: {
            screen: PayWithMoney
        },
        Sell: {
            screen: Sell
        },
        PrepayCoin: {
            screen: PrepayCoin
        },
        PrepayCoinSuccess: {
            screen: PrepayCoinSuccess
        },
        GetCoin: {
            screen: GetCoin
        },
        GetCoinSuccess: {
            screen: GetCoinSuccess
        },
        BuySuccess: {
            screen: BuySuccess
        },
        Market: {
            screen: Market
        },
        Exchange: {
            screen: Exchange
        },
        Phone: {
            screen: Phone
        },
        Bank: {
            screen: Bank
        },
        Buy: {
            screen: Buy
        },
        OpenCard: {
            screen: OpenCard
        },
        Success: {
            screen: Success
        }
    },
    {
        navigationOptions: () => ({
            headerTitleStyle: {
                fontWeight: 'normal',
            },
        }),
    }
);

const Icon = ({focused, file, selectedFile}) => {
    return (
        <Image
            source={focused ? selectedFile : file}
            resizeMode='contain'
            style={focused ? styles.selected_icon : styles.icon}
        />
    )
};

const styles = StyleSheet.create({
    selected_icon: {
        width: 22,
        height: 30
    },
    icon: {
        width: 22,
        height: 22
    }
});