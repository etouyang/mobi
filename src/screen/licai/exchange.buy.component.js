import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, Dimensions } from 'react-native';
import {NavigationActions} from "react-navigation";
import {connect} from "react-redux";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Layout} from "../../config/configDefault";
import {Overlay} from "teaset";

const backImg = require('../../assets/mobai/icon_return.png');
const addImg = require('../../assets/mobai/icon_add.png');
const reduceImg = require('../../assets/mobai/icon_reduce.png');

const accMul = (arg1,arg2) => {
    let m=0,s1=arg1.toString(),s2=arg2.toString();
    try{
        m+=s1.split(".")[1].length
    }catch(e){}
    try{
        m+=s2.split(".")[1].length
    }catch(e){}
    return Number(s1.replace(".",""))*Number(s2.replace(".",""))/Math.pow(10,m)
};


class ExchangeBuyContainer extends Component {
    static navigationOptions = {header: null};
    state = {
        click: true,
        key:null,
        number:null,
        price:null,
    };

    _overlayView = () => {
        return (
            <Overlay.View
                style={{alignItems: 'center', justifyContent: 'center'}}
                modal={true}
                overlayOpacity={0.5}
            >
                <View style={{
                    backgroundColor: '#FFFFFF',
                    width: Layout.width - 60,
                    height: 360,
                    borderRadius: 8
                }}>
                    <View style={{marginTop:30, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:16, color:'#000', fontWeight:'bold'}}>挂单需要完成下面步骤</Text>
                    </View>
                    <View style={{marginTop:30, marginLeft:20, marginRight:20, height:40, backgroundColor:'#E1E5EB', borderRadius:8, justifyContent:'center'}}>
                        <Text style={{marginLeft:10, fontSize:14, color:'#000'}}>1、开户百信银行</Text>
                    </View>
                    <View style={{marginTop:20, marginLeft:20, marginRight:20, height:40, backgroundColor:'#E1E5EB', borderRadius:8, justifyContent:'center'}}>
                        <Text style={{marginLeft:10, fontSize:14, color:'#000'}}>2、环境交易所授权</Text>
                    </View>
                    <TouchableOpacity style={{marginTop:115, marginLeft:20, marginRight:20, height:48, backgroundColor:'#E23D0D', borderRadius:24, alignItems:'center', justifyContent:'center'}}
                                      onPress={() => {
                                          Overlay.hide(this.state.key);
                                          this.props.openCard();
                                      }}>
                        <Text style={{fontSize:14, color:'#FFF'}}>前往开户百信银行</Text>
                    </TouchableOpacity>
                </View>
            </Overlay.View>
        )
    };




    render() {
        return (
            <KeyboardAwareScrollView style={styles.view} showsVerticalScrollIndicator={false}>
                <View style={{width: '100%', height: 65, flexDirection: 'row'}}>
                    <TouchableOpacity style={{marginTop: 30, marginLeft: 20}} onPress={() => this.props.back()}>
                        <Image source={backImg} style={{width: 25, height: 25}}/>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:10, height:29, flexDirection:'row', justifyContent:'space-around'}}>
                    <TouchableOpacity style={{flex:1,alignItems:'center', borderBottomWidth:1, borderColor:'#E23D0D'}}>
                        <Text style={{fontSize:14, color:'#E23D0D'}}>售出</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex:1, alignItems:'center'}}>
                        <Text style={{fontSize:14, color:'rgba(0,0,0,0.4)'}}>买入</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:10, marginLeft:10, marginRight:10, height:231, borderRadius:8, backgroundColor:'#FFF'}}>
                    <View style={{height:38, flexDirection:'row', alignItems:'center', justifyContent:'space-between', borderBottomWidth:1, borderColor:'rgba(0,0,0,0.05)'}}>
                        <Text style={{fontSize:14, color:'#000', marginLeft:12}}>今日推荐交易价格：￥12.90</Text>
                        <TouchableOpacity>
                            <Text style={{fontSize:14, color:'#E23D0D', marginRight:12}}>近7天交易价格</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{marginLeft:20, marginRight:20, marginTop:10, height:48, borderRadius:8, backgroundColor:'#E1E5EB'}}>
                        <TextInput
                            placeholder="出售数量"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            value={this.state.number}
                            onChangeText={(text) => this.setState({number: text})}
                            keyboardType={'numeric'}/>
                    </View>
                    <Text style={{marginTop:6, marginLeft:20, fontSize:12, color:'rgba(0,0,0,0.6)'}}>当前拥有数量：10.00</Text>
                    <View style={{marginLeft:20, marginRight:20, marginTop:20, height:48, borderRadius:8, backgroundColor:'#E1E5EB'}}>
                        <TextInput
                            placeholder="出售单价（RMB）"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            value={this.state.price}
                            onChangeText={(text) => this.setState({price: text})}
                            keyboardType={'numeric'}/>
                    </View>
                    {this.state.number && this.state.price  && <Text style={{marginTop:6, marginLeft:20, fontSize:12, color:'#E23D0D'}}>{'售出总价：' + accMul(this.state.number, this.state.price)}</Text>}
                </View>
                <View style={{marginLeft:20, marginRight:20, marginTop:20, height:44, borderRadius:8, alignItems:'center', justifyContent:'center', backgroundColor:'#E1E5EB'}}>
                    <Text style={{fontSize:14, color:'#000'}}>售出金额到帐：百信银行（尾号：8888）</Text>
                </View>
                <View style={{flex:1, marginTop:164, justifyContent:'flex-end'}}>
                    <TouchableOpacity style={{marginLeft:30, marginRight:30, marginBottom:20, height:48, borderRadius:24, backgroundColor:'#E23D0D', alignItems:'center', justifyContent:'center'}}
                                      onPress={() => {
                                          let key = Overlay.show(this._overlayView());
                                          this.setState({key});
                                      }}>
                        <Text style={{fontSize:14, color:'#FFF'}}>授权环交所并委托挂单</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginBottom:10, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontSize:12, color:'#E23D0D'}}>出售说明</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: '#F8F9FB'
    },
    amount: {
        width:'70%',
        marginLeft:20,
        fontSize:14,
        height:48,
        fontWeight:'bold',
        textAlign: 'left',
        color:'#E23D0D',
        backgroundColor:'#E1E5EB'
    },
});

const mapStateToProps = (state) => {
    return {
        accounts: state.navigation.nav
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        back: () => dispatch(NavigationActions.back()),
        openCard: () => dispatch(NavigationActions.navigate({routeName:'OpenCard'}))
    }
};

export default ExchangeBuy = connect(mapStateToProps,mapDispatchToProps)(ExchangeBuyContainer)