import React, {Component} from 'react';
import { View, Text, Image, StyleSheet} from 'react-native';

const bitImage = require('../../assets/images/bitcoin.png');
export default class BuySuccess  extends React.Component{

    static navigationOptions = {title: '确认订单'};
    render() {
        return (
            <View style={{flex:1, alignItems:'center'}}>
                <Image source={bitImage} style={{width:120, height:120, marginTop:100}}/>
                <Text style={{color:'#fb5c14', marginTop:15}}>订单提交成功, 请留意您的理财产品到期时间</Text>
            </View>
        )
    }
}