import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput } from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from "react-redux";
import {NavigationActions} from "react-navigation";
import {Layout} from "../../config/configDefault";
import ModalDropDown from '../../helpFuc/ModalDropDown';

const downImg = require('../../assets/images/icon_downloop.png');
class BuyContainer extends Component{
    static navigationOptions = {title: '购买理财产品', headerBackTitle: null};
    state = {
        currentPay: 'BTC',
        pays: ['BTC', 'ETH'],
        transferAmount: null
    };

    render() {
        const item = this.props.item;
        return (
            <KeyboardAwareScrollView style={styles.keybordStyle}>
                <View style={styles.container}>
                    <View style={{backgroundColor:'#fdecc4', width:'100%', height:40}}>
                        <Text style={{marginLeft:20, marginTop:2, color:'#ef711a'}}>理财非存款, 实际金额会随着币价的波动而波动, 产品有风险, 购买需谨慎</Text>
                    </View>
                    <Text style={{marginLeft: 20, marginTop:20, fontSize:24}}>{item.title}</Text>
                    <Text style={{color: '#969696', marginLeft:20, marginTop:5}}>{'可购买范围: ' +item.btc_price + ' / ' + item.eth_price}</Text>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, marginTop:10, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{justifyContent:'center', height:50, width:80}}>
                            <Text style={{fontSize: 16}}>付款方式</Text>
                        </View>
                        <View style={styles.amountChoice}>
                            <ModalDropDown
                                ref='transferSelect'
                                options={this.state.pays}
                                defaultValue={this.state.currentPay}
                                textStyle={styles.choiceText}
                                dropdownStyle={styles.dropDownStyle}
                                onSelect={(idx, value) => {
                                    this.setState({currentPay: value}
                                    )}}
                            />
                            <TouchableOpacity onPress={()=>this.refs.transferSelect._onButtonPress()} style={styles.choiceBetween}>
                                <Image source={downImg} style={styles.choiceImg}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>可用余额</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.001BTC</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>购买金额</Text>
                        </View>
                        <TextInput
                            placeholder="金额"
                            style={styles.amount}
                            underlineColorAndroid={'transparent'}
                            keyboardType={'numeric'}
                            onChangeText={(text) => this.setState({transferAmount: text})}
                            value={this.state.transferAmount}
                        />
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>开始时间</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.001BTC</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{width:Layout.width-40, height:50, marginLeft:20, flexDirection:'row'}}>
                        <View style={{flex:1, justifyContent:'center'}}>
                            <Text style={{fontSize: 16}}>结束时间</Text>
                        </View>
                        <View style={{flex:2, justifyContent:'center', alignItems:'flex-end'}}>
                            <Text style={{fontSize: 16}}>0.001BTC</Text>
                        </View>
                    </View>
                    <View style={{width:Layout.width-40, height:1, marginLeft:20, backgroundColor:'#efefef'}}/>
                    <View style={{flex:1, justifyContent:'flex-end'}}>
                        <TouchableOpacity style={styles.sendBtn} onPress={()=>this._transfer()}>
                            <Text style={styles.sendText}>购买</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        )
    };

    _transfer = () => {
        this.props.transfer();
    }
}

const mapStateToProps = (state) => {
    return {
        item: state.licai.buyItem
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        transfer: () => dispatch(NavigationActions.navigate({routeName:'BuySuccess'}))
    }
};

export default Buy = connect(mapStateToProps,mapDispatchToProps)(BuyContainer)

const styles = StyleSheet.create({
    keybordStyle: {
        backgroundColor:'#F6F9FC'
    },
    container: {
        width:Layout.width,
        height:Layout.height - 60,
    },
    amountChoice: {
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        alignItems:'center',
        marginRight:20
    },
    choiceText: {
        color:'#415263',
        fontSize:16
    },
    dropDownStyle: {
        width: 100
    },
    choiceBetween: {
        marginLeft:3
    },
    choiceImg: {
        width:9,
        height:7
    },
    amount: {
        width:Layout.width-150,
        fontSize:16,
        textAlign: 'right',
        color:'#353A41'
    },
    sendBtn: {
        width:Layout.width-40,
        height:45,
        marginLeft:20,
        marginBottom:20,
        borderRadius:22.5,
        backgroundColor:'#ef711a',
        alignItems:'center',
        justifyContent:'center'
    },
    sendText: {
        color:'#FFFFFF',
        fontSize:16
    }
});