import React, {Component} from 'react';
import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import {Layout} from "../../config/configDefault";
import {connect} from "react-redux";
import {NavigationActions} from 'react-navigation';
import {licaiAction} from "../../observable/licai/licai.action";

class LicaiContainer extends Component {
    static navigationOptions = {header:null};

    render() {
        return (
            <View style={styles.container}>

            </View>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#F8F9FB'
    }
});

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        buy: (payload) => dispatch(licaiAction.buyInit(payload))
    }
};

export default Licai = connect(mapStateToProps,mapDispatchToProps)(LicaiContainer)