import { Observable } from 'rxjs/Observable';
import { http, httpJson } from '../helpFuc/http';
import { URL } from '../config/configDefault';
export const api = {
    defaultRetryConfig: {
        max: 100000,
        interval: 1000,
    },
    LoginService: (url, body, headers) => {
        return Observable.fromPromise(
            httpJson.post(URL.base_url + url, {body, headers})
        )
    },
    getToken: (url,headers ) => {
        return Observable.fromPromise(
            http.get(URL.base_url + url, {headers}).then(res => res.json())
        )
    }

};