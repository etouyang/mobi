import { Observable, Subject } from 'rxjs';
import {WebSocketSubject} from 'rxjs/webSocket';
import { tryCatch } from 'rxjs/internal/util/tryCatch';
import { errorObject } from 'rxjs/internal/util/errorObject';

export const isCleanClose = (event) =>
    event.type === 'close' && event.wasClean === true;

const createTransformedInput = (transformInput, webSocketSubject) => {
    function nextTransformed(data) {
        // use `tryCatch` as in WebSocketSubject.
        // error should be passed to `webSocketSubject._output` but thats "private"
        const result = tryCatch(transformInput)(data);
        if (result === errorObject) {
            webSocketSubject.error(errorObject.e);
        } else {
            webSocketSubject.next(result);
        }
    }

    return {
        next: nextTransformed.bind(webSocketSubject),
        complete: webSocketSubject.complete.bind(webSocketSubject),
        error: webSocketSubject.error.bind(webSocketSubject),
    };
};

const createTransformedOutputResultSelector = transformOutput => (messageEvent) => transformOutput(messageEvent.data);

const identity = (value) => value;

export const createObservableWebSocket = ({
                                              url,
                                              protocol,
                                              transformInput,
                                              transformOutput,
                                          }) => {
    const open = new Subject();
    const close = new Subject();

    const webSocketSubject = new WebSocketSubject({
        url,
        protocol,
        openObserver: open,
        closeObserver: close,
        resultSelector:
            typeof transformOutput === 'function'
                ? createTransformedOutputResultSelector(transformOutput)
                : // revert `WebSocketSubject.resultSelector` defaulting to `JSON.parse`
                identity,
    });

    return {
        input:
            typeof transformInput === 'function'
                ? createTransformedInput(transformInput, webSocketSubject)
                : webSocketSubject,
        output: webSocketSubject,
        open: open.asObservable(),
        close: close.asObservable(),
    };
};

export const createObservableWebSocketJson = ({
                                                  transformInput = JSON.stringify,
                                                  transformOutput = JSON.parse,
                                                  ...rest
                                              }) =>
    createObservableWebSocket({
        ...rest,
        transformInput,
        transformOutput,
    });