import { combineEpics } from 'redux-observable';
import ActionType from '../../helpFuc/actionType';
import { Observable } from 'rxjs';
import { NavigationActions } from 'react-navigation';

const buyInitEpic = (actions) =>
    actions.ofType(ActionType.BUY_INIT)
        .map(()=> NavigationActions.navigate({routeName: 'Buy'}));

export const licaiEpic = combineEpics(buyInitEpic);