import ActionType from '../../helpFuc/actionType'
export const licai = (state = {}, action) => {
    switch (action.type) {
        case ActionType.BUY_INIT: {
            return {
                ...state,
                buyItem: action.payload
            }
        }
        default:
            return state
    }
};