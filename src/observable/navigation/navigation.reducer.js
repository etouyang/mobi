import  AppNavigator  from '../../screen/navigation/navigator';
import { combineReducers } from 'redux';

export const nav = (state, action) => {
    // TODO: This reducer is called before AppNavigator is even existing?
    if (!AppNavigator) {
        return null;
    }

    const nextState = AppNavigator.router.getStateForAction(action, state);
    return (
        nextState ||
        state ||
        AppNavigator.router.getStateForAction(
            AppNavigator.router.getActionForPathAndParams('Tab')
        )
    );
};

export const navigation = combineReducers({nav});