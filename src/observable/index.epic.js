import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/publish';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/repeatWhen';
import 'rxjs/add/operator/retry'
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/publishReplay';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/throw';

import { combineEpics } from 'redux-observable';
import { commonEpic } from '../observable/common/common.epic';
import { licaiEpic } from '../observable/licai/licai.epic';
import { accountEpic } from '../observable/account/account.epic';

export const epic = () => combineEpics(
    commonEpic,
    licaiEpic,
    accountEpic
);
