import ActionType from '../../helpFuc/actionType'
export const account = (state = {}, action) => {
    switch (action.type) {
        case ActionType.ACCOUNT_LOGIN_SUCCESS:
        case ActionType.ACCOUNT_REFRESH_TOKEN_SUCCESS: {
            return {
                ...state,
                token: action.payload
            }
        }
        default:
            return state
    }
};