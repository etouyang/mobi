import { createPayloadAction } from '../../helpFuc/payload-action';
import ActionType from '../../helpFuc/actionType';

const login = createPayloadAction(ActionType.ACCOUNT_LOGIN);
const loginSuccess = createPayloadAction(ActionType.ACCOUNT_LOGIN_SUCCESS);
const loginFail = createPayloadAction(ActionType.ACCOUNT_LOGIN_FAIL);

const refreshToken = createPayloadAction(ActionType.ACCOUNT_REFRESH_TOKEN);
const refreshTokenSuccess = createPayloadAction(ActionType.ACCOUNT_REFRESH_TOKEN_SUCCESS);
const refreshTokenFail = createPayloadAction(ActionType.ACCOUNT_REFRESH_TOKEN_FAIL);

export const accountAction = {
    login,
    loginSuccess,
    loginFail,
    refreshToken,
    refreshTokenSuccess,
    refreshTokenFail
};