import { combineEpics } from 'redux-observable';
import ActionType from '../../helpFuc/actionType';
import { Observable } from 'rxjs';
import { URL } from '../../config/configDefault';

const loginEpic = (actions, store, {LoginService}) =>
    actions.ofType(ActionType.SYSTEM_INIT)
        .mergeMap(({payload}) =>
                LoginService( URL.login_url, payload )
                    .switchMap(res => {
                        console.log(res);
                        return Observable.empty();
                    })
                    .catch(err => {
                        console.log(err);
                        return Observable.empty();
                    })
        );

const refreshTokenEpic = (actions, store, {getToken}) =>
    actions.ofType(ActionType.ACCOUNT_REFRESH_TOKEN)
        .mergeMap(({payload}) =>
            getToken(URL.refresh_token_url, {token: payload, app:'react native'})
                .switchMap(res => {
                    console.log(res);
                    return Observable.empty();
                })
                .catch(err => {
                    console.log(err);
                    return Observable.empty();
                })
        );

export const accountEpic = combineEpics(
    loginEpic,
    refreshTokenEpic
);