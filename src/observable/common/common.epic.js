import { combineEpics } from 'redux-observable';
import ActionType from '../../helpFuc/actionType';
import { Observable } from 'rxjs';
import { accountAction } from '../index.action';

const systemEpic = (actions, store, {getToken}) =>
    actions.ofType(ActionType.SYSTEM_INIT)
        .map(()=>accountAction.login({"name":"hello", "password":"1234"}));

export const commonEpic = combineEpics(systemEpic);