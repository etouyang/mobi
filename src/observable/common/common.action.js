import { createPayloadAction } from '../../helpFuc/payload-action';
import ActionType from '../../helpFuc/actionType';

const systemInit = createPayloadAction(ActionType.SYSTEM_INIT);

export const commonAction = {
    systemInit
};

