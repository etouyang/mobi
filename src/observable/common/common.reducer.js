import ActionType from '../../helpFuc/actionType'
export const common = (state = {}, action) => {
    switch (action.type) {
        case ActionType.SYSTEM_INIT: {
            return {
                ...state,
                test: action.payload
            }
        }
        default:
            return state
    }
};