import { common } from './common/common.reducer';
import { navigation } from './navigation/navigation.reducer';
import { licai } from './licai/licai.reducer';
import { account } from './account/account.reducer'

export const reducer = {
    common,
    navigation,
    licai,
    account
};