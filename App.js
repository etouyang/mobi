/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import {configureStore} from './src/config/configStore';
import { commonAction } from './src/observable/index.action';
import {AppNavigatorRedux} from './src/screen/navigation/navigator.container';

const {store, persistor} = configureStore();

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
        <Provider store={store}>
          <PersistGate onBeforeLift={() => store.dispatch(commonAction.systemInit())}
                       persistor={persistor}>
              <AppNavigatorRedux/>
          </PersistGate>
        </Provider>
    )
  }
}
