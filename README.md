1.
执行npm install
2.
首先将mobi/node_modules/native-echarts/src/components/Echarts/index.js 文件里面的代码替换为下面的代码:
import React, { Component } from 'react';
import { WebView, View, StyleSheet, Dimensions } from 'react-native';
import renderChart from './renderChart';
import echarts from './echarts.min';

const {width, height} = Dimensions.get('window');

export default class App extends Component {
componentWillReceiveProps(nextProps) {
if(nextProps.option !== this.props.option) {
this.refs.chart.reload();
}
}

render() {
return (
<View style={{width:width, height: this.props.height || 400}}>
<WebView
ref="chart"
scrollEnabled = {true}
injectedJavaScript = {renderChart(this.props)}
style={{
width:width + 170,
height: this.props.height || 400,
left:-85,
backgroundColor: this.props.backgroundColor || 'transparent'
}}
scalesPageToFit={true}
bounce={false}
source={require('./tpl.html')}
onMessage={event => this.props.onPress ? this.props.onPress(JSON.parse(event.nativeEvent.data)) : null}
/>
</View>
);
}
}


3.
将mobi/node_modules/react-native-linear-gradient/android/build.gradle 文件里面的代码替换为下面的代码
apply plugin: 'com.android.library'

def DEFAULT_COMPILE_SDK_VERSION             = 26
def DEFAULT_BUILD_TOOLS_VERSION             = "26.0.3"
def DEFAULT_MIN_SDK_VERSION                 = 16
def DEFAULT_TARGET_SDK_VERSION              = 26

android {
compileSdkVersion rootProject.hasProperty('compileSdkVersion') ? rootProject.compileSdkVersion : DEFAULT_COMPILE_SDK_VERSION
buildToolsVersion rootProject.hasProperty('buildToolsVersion') ? rootProject.buildToolsVersion : DEFAULT_BUILD_TOOLS_VERSION

defaultConfig {
minSdkVersion rootProject.hasProperty('minSdkVersion') ? rootProject.minSdkVersion : DEFAULT_MIN_SDK_VERSION
targetSdkVersion rootProject.hasProperty('targetSdkVersion') ? rootProject.targetSdkVersion : DEFAULT_TARGET_SDK_VERSION
versionCode 1
versionName "1.0"
}
}

dependencies {
compileOnly "com.facebook.react:react-native:+"
}

4.
运行工程 
苹果手机命令: react-native run-ios
安卓手机命令: react-native run-android


